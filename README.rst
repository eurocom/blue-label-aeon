=============================
Python Blue Label API Wrapper
=============================

This is a thin Python wrapper for the Blue Label API.  Basic usage looks like this::

Airtime Example:
----------------

  >>>  from aeon.airtime import AeonAirtime
  >>>
  >>>  aeon = AeonAirtime(
  >>>      tran_type=AeonAirtime.TRAN_TYPE_8TA,
  >>>      host=<AEON_HOST>,
  >>>      port=<AEON_PORT>,
  >>>      user_pin=<AEON_USER_PIN>,
  >>>      device_id=<AEON_DEVICE_ID>,
  >>>      device_serial=<AEON_DEVICE_SERIAL>
  >>>  )
  >>>
  >>>  airtime_resp = aeon.do_topup(
  >>>      phone_no=<TOPUP_PHONE_NO>,
  >>>      amount=<TOPUP_AMOUNT>,
  >>>      product_name=<TOPUP_PRODUCT_NAME>,
  >>>      authoriser=<AUTHORISER>
  >>>  )
  >>>
  >>>  if airtime_resp.has_error():
  >>>      pass
  >>>  else:
  >>>      pass

Electricity Example:
--------------------

  >>>  from aeon.electricity import AeonElectricity
  >>>
  >>>  aeon = AeonElectricity(
  >>>      host=<AEON_HOST>,
  >>>      port=<AEON_PORT>,
  >>>      device_id=<AEON_DEVICE_ID>,
  >>>      device_serial=<AEON_DEVICE_SERIAL>,
  >>>      user_pin=<AEON_USER_PIN>,
  >>>      meter_no=<METER_NO>,
  >>>      ignore_debts=<True/False>,
  >>>      ignore_free_tokens=<True/False>
  >>>  )
  >>>
  >>>  electricity_resp = aeon.get_voucher(
  >>>      product_name=<ELECT_PRODUCT_NAME>,
  >>>      authoriser=<AUTHORISER>
  >>>  )
  >>>
  >>>  if electricity_resp.has_error():
  >>>      pass
  >>>  else:
  >>>      pass