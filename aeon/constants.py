"""
A simple file to hold some constants needed for the project like XML structure etc.
"""
##
# The XML needed to do to get an authorised session id for any transaction created through Blue Label. The following
# is needed to do the request.
#
#   - user_pin
#   - device_id
#   - device_serial
#   - tran_type
#   - ref
##
TOPUP_AUTH_XML = (
    "<request>"
        "<EventType>Authentication</EventType>"
        "<event>"
            "<UserPin>%(user_pin)s</UserPin>"
            "<DeviceId>%(device_id)s</DeviceId>"
            "<DeviceSer>%(device_serial)s</DeviceSer>"
            "<TransType>%(tran_type)s</TransType>"
            "<Reference>%(ref)s</Reference>"
        "</event>"
    "</request>\n"
)

##
# The XML needed to do an airtime top-up request. The following is needed to do the request:
#
#   - session_id
#   - ref
#   - phone_no
#   - amount
#   - datetime
#   - date
#   - authoriser
#   - product_name
##
TOPUP_XML = (
    "<request>"
        "<SessionId>%(session_id)s</SessionId>"
        "<EventType>GetTopup</EventType>"
        "<event>"
            "<Reference>%(ref)s</Reference>"
            "<PhoneNumber>%(phone_no)s</PhoneNumber>"
            "<Amount>%(amount).2f</Amount>"
            "<Recon batchNumber='0' "
                   "terminalId='0' "
                   "merchantId='0' "
                   "transNumber='0' "
                   "transReference='0' "
                   "sysReference='0' "
                   "transDateTime='%(datetime)s' "
                   "businessDate='%(date)s' "
                   "transType='01' "
                   "accountNumber='0' "
                   "productId='0' "
                   "amount='500' "
                   "authoriser='%(authoriser)s' "
                   "productName='%(product_name)s' />"
        "</event>"
    "</request>\n"
)

##
# The XML needed to do a reprint of an airtime topup. The following is needed to do the request:
#
#   - session_id
#   - reference
#   - phone_no
##
TOPUP_REPRINT_XML = (
    "<request>"
        "<SessionId>%(session_id)s</SessionId>"
        "<EventType>Reprint</EventType>"
        "<event>"
            "<TransRef />"
            "<OrigReference>%(reference)s</OrigReference>"
            "<PhoneNumber>%(phone_no)s</PhoneNumber>"
        "</event>"
    "</request>\n"
)

##
# The XML needed to do an authorization and meter confirmation:
#
#   - device_id
#   - device_serial
#   - user_pin
#   - meter_no
#   - amount
#   - ref
##
ELEC_METER_CONFIRM_XML = (
    "<request>"
        "<EventType>ConfirmMeter</EventType>"
        "<event>"
            "<UserPin>%(user_pin)s</UserPin>"
            "<DeviceId>%(device_id)s</DeviceId>"
            "<DeviceSer>%(device_serial)s</DeviceSer>"
            "<MeterNum>%(meter_no)s</MeterNum>"
            "<Amount>%(amount)d</Amount>"
            "<Reference>%(ref)s</Reference>"
        "</event>"
    "</request>\n"
)

##
# The XML needed to request a voucher for a customer using the transaction reference returned from the meter
# confirmation request. This method returns meter and token information.
#
#   - session_id
#   - tran_ref
#   - ref
#   - datetime
#   - date
#   - amount
#   - authoriser
#   - product_name
##
ELEC_VOUCHER_REQ_XML = (
    "<request>"
        "<SessionId>%(session_id)s</SessionId>"
        "<EventType>GetVoucher</EventType>"
        "<event>"
            "<Type></Type>"
            "<TransRef>%(tran_ref)s</TransRef>"
            "<Reference>%(ref)s</Reference>"
            "<Recon batchNumber='0' "
                   "terminalId='0' "
                   "merchantId='0' "
                   "transNumber='0' "
                   "transReference='0' "
                   "sysReference='0' "
                   "transDateTime='%(datetime)s' "
                   "businessDate='%(date)s' "
                   "transType='01' "
                   "accountNumber='0' "
                   "productId='0' "
                   "amount='%(amount)d' "
                   "authoriser='%(authoriser)s' "
                   "productName='%(product_name)s' />"
        "</event>"
    "</request>\n"
)

##
# Once a voucher has been printed or confirmed as sold, the server needs to be informed so the transaction can be
# finalized.
#
#   - session_id
#   - tran_ref
#   - ref
##
ELEC_SALE_CONFIRM_XML = (
    "<request>"
        "<SessionId>%(session_id)s</SessionId>"
        "<EventType>SoldVoucher</EventType>"
        "<event>"
            "<TransRef>%(tran_ref)s</TransRef>"
            "<Reference>%(ref)s</Reference>"
        "</event>"
    "</request>\n"
)

##
# This will request an available product list for the specific network. The network will depend on the TransType used
# to authenticate with (Vodacom, CellC, MTN, TelkomMobile, TelkomFixedLine, TelkomWorldCall, and Neotel)
#
#   - session_id
##
DATA_BUNDLE_PRODUCTS_REQ_XML = (
    "<request>"
        "<SessionId>%(session_id)s</SessionId>"
        "<EventType>GetProductList</EventType>"
        "<event>"
            "<Recon batchNumber='0' "
                   "terminalId='0' "
                   "merchantId='0' "
                   "transNumber='0' "
                   "transReference='0' "
                   "sysReference='0' "
                   "transDateTime='%(datetime)s' "
                   "businessDate='%(date)s' "
                   "transType='01' "
                   "accountNumber='0' "
                   "productId='0' "
                   "amount='0' "
                   "authoriser='%(authoriser)s' "
                   "productName='%(product_name)s' />"
        "</event>"
    "</request>\n"
)

##
# This will request a specific Topup bundle from the system. The network will depend on the TransType you used to
# authenticate with (Vodacom, CellC, MTN, and TelkomMobile)
#
#   - session_id
#   - phone_number
#   - product_code
#   - ref
##
DATA_BUNDLE_REQ_XML = (
    "<request>"
        "<SessionId>%(session_id)s</SessionId>"
        "<EventType>DoBundleTopup</EventType>"
        "<event>"
            "<Reference>%(ref)s</Reference>"
            "<PhoneNumber>%(phone_number)s</PhoneNumber>"
            "<ProductCode>%(product_code)s</ProductCode>"
            "<Recon batchNumber='0' "
                   "terminalId='0' "
                   "merchantId='0' "
                   "transNumber='0' "
                   "transReference='0' "
                   "sysReference='0' "
                   "transDateTime='%(datetime)s' "
                   "businessDate='%(date)s' "
                   "transType='01' "
                   "accountNumber='0' "
                   "productId='0' "
                   "amount='%(product_code)s' "
                   "authoriser='%(authoriser)s' "
                   "productName='%(product_name)s' />"
        "</event>"
    "</request>\n"
)
