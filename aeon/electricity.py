import socket
import uuid

from datetime import datetime

from .response import AeonResponse
from .constants import (
    ELEC_METER_CONFIRM_XML, ELEC_SALE_CONFIRM_XML, ELEC_VOUCHER_REQ_XML
)


class AeonElectricity():
    """
    A simple class to the interface with the Blue Label Aeon service. This allows you to buy electricity voucher.
    """
    BUFFER_SIZE = 4096

    def __init__(self, host, port, device_id, device_serial, user_pin, meter_no, amount, ignore_debts=False,
                 ignore_free_tokens=False):
        """
        """
        self.ignore_debts = ignore_debts
        self.ignore_free_tokens = ignore_free_tokens
        self.session_id = ''
        self.tran_ref = ''
        self.response = AeonResponse()

        self.host = host
        self.port = port
        self.data = {
            'user_pin': user_pin,
            'device_id': device_id,
            'device_serial': device_serial,
            'meter_no': meter_no,
            'amount': amount,
            'ref': uuid.uuid1()
        }

        # Messages.
        self.auth_message = ELEC_METER_CONFIRM_XML % self.data

    def __drain_sock(self, sock):
        """
        """
        data = ''
        chunk = sock.recv(self.BUFFER_SIZE)

        while len(chunk):
            data += chunk

            if '\n' not in data:
                chunk = sock.recv(self.BUFFER_SIZE)
            else:
                chunk = ''

        return data

    def __do_call(self, message, confirm_message):
        """
        """
        # Do calls through socket.
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

        try:
            sock.connect((self.host, self.port))

            # First we need to do an Auth Request.
            sock.send(self.auth_message)
            self.response.add_response(xml_string=self.__drain_sock(sock))

            if not self.response.has_error():
                self.data.update({
                    'session_id': self.response.session_id,
                    'tran_ref': self.response.data['TransRef']
                })

                message = message % self.data
                sock.send(message)
                self.response.add_response(xml_string=self.__drain_sock(sock))

                if not self.response.has_error():

                    data_keys = self.response.data.keys()

                    if self.ignore_debts and 'Debts' in data_keys:
                        self.response.is_error = True
                        self.response.error_code = 'EUR-001'
                        self.response.error_text = 'Your meter number cannot be used as it either has debts associated.'

                    else:
                        # Check if there are any free basic electricity tokens associated.
                        if self.ignore_free_tokens and 'BSSTTokens' in data_keys:
                            if self.response.data['BSSTTokens']['BSSTToken'].__len__() > 2:
                                if self.response.data['BSSTTokens']['BSSTToken']['Desc'] == 'Free Basic Electricity':
                                    self.response.is_error = True
                                    self.response.error_code = 'EUR-002'
                                    self.response.error_text = 'Your meter number cannot be used as it has free ' \
                                                               'basic electricity associated to it.'
                            else:
                                for token in self.response.data['BSSTTokens']['BSSTToken']:
                                    if token['Desc'] == 'Free Basic Electricity':
                                        self.response.is_error = True
                                        self.response.error_code = 'EUR-002'
                                        self.response.error_text = 'Your meter number cannot be used as it has free ' \
                                                                   'basic electricity associated to it.'

                        if not self.response.has_error():
                            confirm_message = confirm_message % self.data
                            sock.send(confirm_message)
                            self.response.add_response(xml_string=self.__drain_sock(sock))

            return self.response

        except Exception, e:
            print e

        finally:
            sock.close()

    def get_voucher(self, product_name, authoriser):
        """
        """
        self.data.update({
            'product_name': product_name,
            'datetime': datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
            'date': datetime.now().strftime('%Y-%m-%d'),
            'authoriser': authoriser
        })

        return self.__do_call(
            message=ELEC_VOUCHER_REQ_XML,
            confirm_message=ELEC_SALE_CONFIRM_XML
        )