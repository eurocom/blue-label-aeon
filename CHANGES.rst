=========
Changelog
=========

0.4.5 (2015-06-04)
==================

- added data calls.

0.4.4 (2013-02-25)
==================

- made a change to the tokens to only fail if the description of the token is for Free Basic electricity.

0.4.3 (2013-02-14)
==================

- fixed bug in debt and free based tokens.

0.4.2 (2013-02-14)
==================

- added better socket handling in both electricity and airtime api.
- fixed an attribute error in the electricity api.

0.4.1 (2013-02-14)
==================

- set the error fields for debts or free tokens if they should be ignored.

0.4 (2013-02-14)
================

- made a change to the electricity api to allow tokens to be ignored for debts or free tokens.

0.3 (2013-02-10)
================

- added extra tests to test all the electricity vouchers.
- made the buffer read size larger for the sockets.

0.2 (2013-01-24)
================

- changes the response objects to store all the information.
- updated the README.rst file with some fixes.

0.1 (2013-01-21)
================

- initial release